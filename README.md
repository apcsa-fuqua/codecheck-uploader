# CodeCheck Upload

1. Open this repository in a [Gitpod environment](https://gitpod.io/new#https://gitlab.com/apcsa-lambert/codecheck-uploader)
2. Run `./utilities/initProject [projectName] [--run-btn / '']` to initialize a new project
3. Click `Run` to upload the files to CodeCheck
5. Wait for the result to appear
6. If the upload is successful, copy the public and edit URLs for safekeeping
7. If you wish to update the CodeCheck's, simply click `Run` again, as this repo is now associated with the CodeCheck's it created

### When this repository gets initialized in a Gitpod environment, corresponding links will be placed here.
