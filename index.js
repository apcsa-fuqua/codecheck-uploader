import { readFileSync, writeFileSync, readdirSync } from "fs";
import fetch from "node-fetch";
import { stringify } from "querystring";
import { load } from 'cheerio';

function matprint(mat) {
   let shape = [mat.length, mat[0].length];
   function col(mat, i) {
      return mat.map(row => row[i]);
   }
   let colMaxes = [];
   for (let i = 0; i < shape[1]; i++) {
      colMaxes.push(Math.max.apply(null, col(mat, i).map(n => n.toString().length)));
   }

   let result = "";
   mat.forEach(row => {
      let rowStr = row.map((val, j) => {
         return new Array(colMaxes[j] - val.toString().length + 1).join(" ") + val.toString() + "  ";
      }).join("");
      result += rowStr + "\n";
   });

   return result;
}

function extractEditUrl(htmlString) {
   const $ = load(htmlString);
   let editUrl = '';

   $('a').each((i, link) => {
      const href = $(link).attr('href');

      if (href.includes("private/problem/")) {
         editUrl = href;
         return false;
      }
   });

   return editUrl;
}

function convertToText(htmlString) {
   const $ = load(htmlString);
   let iframeSrc = $('iframe').attr('src');
   let bufferObj = Buffer.from(iframeSrc.replace("data:text/html;base64,", ""), "base64");
   let decodedString = bufferObj.toString("utf8");
   const body = load(decodedString)('body')
   $('iframe').remove();
   $('body').append(body);
   const table = $('table');
   let data = [];

   table.find('tr').each((i, row) => {
      let rowData = [];
      $(row).find('td, th').each((j, cell) => {
         rowData.push($(cell).text().replace("\n", "\\n"));
      });
      data.push(rowData);
   });
   let matrixString = "";
   if (data.length != 0) {
      matrixString = "\n<pre>\n" + matprint(data) + "\n</pre>"
   }
   return load(
      $('body')
         .html()
         .replace(/<table[\S\s]*table>/g, matrixString)
         .replace(`</span><p class="header studentFiles">`, `</span>\n<p class="header studentFiles">`)
         .replace("<br>", "\n")
   )("body").text();
}

async function SendRequest(datatosend, folder) {
   async function OnResponse(isReUpload, response, folder) {
      let html = await response.text();
      console.log("\n" + folder + ":\n-------------------------------------------")

      if (response.status == 200) {
         console.log(convertToText(html) + "\n")
         if (!isReUpload) {
            var edurl = extractEditUrl(html)
            let txt = ""
            try {
               txt = readFileSync('url.txt', 'utf-8')
            } catch { }
            txt = txt + folder + ": " + edurl + "\n";
            writeFileSync('url.txt', txt, 'utf-8')

            let md = ""
            try {
               md = readFileSync('README.md', 'utf-8')
            } catch { }
            md = md + `- \`${folder}\`: ${edurl}\n`;
            writeFileSync('README.md', md, 'utf-8')
         }
      } else {
         console.log(html);
      }
      console.log("-------------------------------------------\n")
   }
   var urlFile;
   try {
      urlFile = readFileSync("./url.txt", "utf8");
   } catch (e) {
      urlFile = null;
   }
   const inputString = urlFile ?? "";
   const lines = inputString.split('\n');
   const resultObject = {};
   lines.forEach(line => {
      const key = line.substring(0, line.indexOf(':'))
      const value = line.substring(line.indexOf(':') + 1)
      resultObject[key.trim()] = value.trim();
   });

   var url = Object.keys(resultObject).includes(folder) ? resultObject[folder].replace("private/problem", "editedFiles") : "https://codecheck.io/uploadFiles";



   fetch(url, {
      body: datatosend,
      method: "POST",
      headers: {
         "Content-Type": "application/x-www-form-urlencoded",
      },
   }).then(res => { OnResponse(Object.keys(resultObject).includes(folder), res, folder) });
}

var folders = readdirSync("./projects");
for (var folder of folders) {
   var i = 1;
   var jsonContext = {};
   readdirSync("./projects/" + folder).forEach((file) => {
      jsonContext["filename" + i] = file;
      jsonContext["contents" + i] = readFileSync("./projects/" + folder + "/" + file, "utf8");
      i++;
   });

   var encodedPayload = stringify(jsonContext);
   encodedPayload = encodedPayload.replace(/%20/g, "+");
   encodedPayload = encodedPayload.replace(/%0A/g, "%0D%0A");
   encodedPayload = encodedPayload.replace(/\(/g, "%28");
   encodedPayload = encodedPayload.replace(/\)/g, "%29");
   console.log(`Uploading "${folder}"`);
   SendRequest(encodedPayload, folder);
}